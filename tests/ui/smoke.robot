*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${URL}      http://janegermaier.gitlab.io/csws2020-02/
${HOST}     False
${BROWSER}  Chrome  # Firefox

${TITLE}       Hello World!

*** Test Cases ***
Check H1 Title
    Open Browser  ${URL}  ${BROWSER}  remote_url=${HOST}  options=add_argument("--ignore-certificate-errors")
    Wait Until Page Contains  ${TITLE}
